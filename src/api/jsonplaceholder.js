import axios from 'axios'

const url = 'https://jsonplaceholder.typicode.com/';

const api = {
    getUsersList() {
        return axios.get(`${url}users`)
    },
    getAllPosts() {
        return axios.get(`${url}posts`)
    },
    getComments() {
        return axios.get(`${url}comments`)
    },
    saveComment(postId, text) {
        return axios.post(`${url}comments/${postId}/comments`, {
            name: '',
            email: '',
            body: text,
        })
    }
}

export default api